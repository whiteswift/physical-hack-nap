export const getImgUrl = (urlTemplate, shot, size) => {
    return urlTemplate.replace('{{scheme}}', '')
                  .replace('{{shot}}', shot)
                  .replace('{{size}}', size);
}
