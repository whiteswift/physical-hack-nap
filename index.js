import express from 'express';
import exphbs from 'express-handlebars';
import path from 'path';
import { init, ymal, htw, detail } from './middleware/lad';
import landingPage from './controller';
const app = express();

app.use(express.static(path.join(__dirname, './public/')));
app.set('views', path.join(__dirname, '/views'));
app.engine(
    '.hbs',
    exphbs({
        extname: '.hbs',
        defaultLayout: 'main',
        layoutsDir: path.join(__dirname, './views/layouts/')
    })
);

app.set('view engine', '.hbs');

app.get('/', landingPage);

app.get('/test', (req, res) => {
    res.render('test');
});

app.get('/:pid', init, detail, ymal, htw, (req, res) => {
    res.locals.headerScripts = `
        <script>window.hackData = ${JSON.stringify(res.locals)
            .replace(/</g, '\\u003c')
            .replace('\u2028', '\\u2028')
            .replace('\u2029', '\\u2029')}</script>
    `;
    res.render('index');
});

app.get('/api/:pid', init, detail, ymal, htw, (req, res) => {
    res.send(res.locals);
});

app.get('/favicon.ico', function(req, res) {
    res.status(204);
});

app.listen(3000, function() {
    console.log(`App running at port 3000`);
});
