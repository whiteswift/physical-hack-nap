# Personal Assistant; by MR PORTER

The benefits of having instant access to online resources is sometimes lost when you visit a physical store. Case and point; how many times have you wondered what those jeans would look good with, but haven't bought them for a lack of creativity?

Wonder no more, a group of erudite young developers and designers have set off to improve this neglected experience.

Personal assistant; by MR PORTER - a purpose built web application to enrich your in-store shopping experience, with pragmatic outfit inspiration, products you may also like, and directions to these products - so these trips into a physical store don't seem so much of an expedition.

## Details

This project is an improvement to the luxury shopping experience, by using physical devices.

NFC stickers are put on the hangers of products in store; when tapped with your phone, it will navigate to a special outfit inspiration page.

Bluetooth beacons using the Eddystone protocol suppliment this experience by suggesting size guides, alteration services, and offers when it makes contextual sense. They are placed in specific locations around the store to provide additional information, when the customer will most find it useful. They will receive a passive bluetooth notification when they are near the beacon device.

Credits to myself, Gray Jones and Lidan Liu.

![Personal Assistant; by MR PORTER](https://bitbucket.org/whiteswift/physical-hack-nap/raw/master/public/images/readme_image.png)

## To install and run

`nvm use`

`yarn install`

`npm start`

Navigate to a product assistant page:

- [http://localhost:3000](http://localhost:3000)

- [http://localhost:3000/[productID]](http://localhost:3000)

E.g. A Maison Kitsuné T-Shirt has a product ID of 920431 on the MR PORTER website. So navigate to http://localhost:3000/920431 to replicate the behaviour of tapping on a product in-store.

## Demo

Navigate to [https://server-xjsgcpeydm.now.sh/919658](https://server-xjsgcpeydm.now.sh/919658)

## Licence

MIT