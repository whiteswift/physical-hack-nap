document.addEventListener('DOMContentLoaded', startup, false);

function onShareClick() {
    // WEB SHARE API
    navigator
        .share({
            title: 'Checkout this funny photo',
            text: 'rahahahahahaha',
            url: 'https://google.com' // url: window.location.origin + '/' + photo.id
        })
        .then(() => console.log('Successfully shared'))
        .catch(error => console.log('Error sharing:', error));
}

function startup() {
    var modal = document.getElementById('modal');
    keyDwonHandler(modal);

    document.querySelector('.close').addEventListener('click', e => {
        toggleModal(modal);
    });

    document.querySelectorAll('.outfits img').forEach((el, index) => {
        el.addEventListener('click', e => {
            window.scrollTo(0, 0);
            updateModal(modal, index);
            toggleModal(modal);
        });
    });
}

function updateModal(modal, index) {
    updateMainPid(modal, index);

    var others = modal.querySelector('.other-pids');

    others.querySelectorAll('img').forEach((el, i) => {
        el.src = hackData.outfits[i].mainImgUrl;
    });

    others.querySelectorAll('.product-brand').forEach((el, i) => {
        el.innerHTML = hackData.outfits[i].brand.name;
    });

    others.querySelectorAll('.product-description').forEach((el, i) => {
        el.innerHTML = hackData.outfits[i].name;
    });

    updateOval(others, index);

    others.querySelectorAll('.outfit-pid').forEach((el, i) => {
        el.addEventListener('click', e => {
            updateMainPid(modal, i);
            updateMap(modal, i);
            updateOval(others, i);
        });
    });
}

function updateMap(modal, index) {
    const mapIndex = 0 < index && index < 5 ? index : 4;
    modal.querySelector('.map').src = `/maps/${mapIndex}.png`;
}

function updateMainPid(modal, index) {
    modal.querySelector('.main-product-image').src = hackData.outfits[index].mainImgUrl;
    modal.querySelector('.main-product-des .product-brand').innerHTML = hackData.outfits[index].brand.name;
    modal.querySelector('.main-product-des .product-description').innerHTML = hackData.outfits[index].name;
}

function updateOval(others, index) {
    others.querySelectorAll('.modal-oval').forEach((el, other_index) => {
        if (index === other_index) {
            el.classList.remove('hollow');
            el.classList.add('yellow');
        } else {
            el.classList.add('hollow');
            el.classList.remove('yellow');
        }
    });
}

function toggleModal(modal) {
    modal.classList.toggle('modal-hide');
    // Changes opacity of the modal
    if (modal.classList.contains('hide-modal')) {
        // Show and animate
        modal.classList.remove('slide-out');
        modal.classList.add('slide-top');

        modal.classList.remove('hide-modal');
        modal.classList.add('show-modal');
    } else {
        // Hide and animate

        modal.classList.remove('slide-top');
        modal.classList.add('slide-out');

        modal.classList.remove('show-modal');
        modal.classList.add('hide-modal');
    }
}

function keyDwonHandler(modal) {
    window.addEventListener('keydown', event => {
        if (event.keyCode === 32) {
            // Spacebar
            event.preventDefault();
            toggleModal(modal);
        }
    });
}
