import fetch from 'isomorphic-fetch';
import { getImgUrl } from '../utils';

const init = (req, res, next) => {
    res.locals = res.locals || {};
    next();
}

const fetchAll = (res, next,pids, type) => {
    // console.log('all pids: ',pids);
    const allPids = pids.map(pid => {
        let url = `https://api.net-a-porter.com/MRP/GB/en/detail/${pid}`;
        return fetch(url);
    });

    let allFetch = [];

    Promise.all(allPids)
    .then(productData => {
        productData.forEach(pd => {
            allFetch.push(pd.json());
        });

        Promise.all(allFetch)
        .then(allJson => {
            const transformedJson = allJson.map(json => pidReducer(json));
            res.locals[type] = transformedJson;
            next();
        });
    }).catch(err => {
        console.log(err);
    });
}

const ymal = (req, res, next) => {
    const pid = req.params.pid;
    fetch(`https://www.mrporter.com/api/styling/products/${pid}/5/ymal`)
    .then(data => data.json())
    .then(jsonData => {
        // res.locals.ymalPids = jsonData.ymalPids;
        fetchAll(res, next, jsonData.ymalPids, 'ymal')
    });
}

const htw = (req, res, next) => {
    const pid = req.params.pid;
    fetch(`https://www.mrporter.com/api/styling/products/${pid}/5/outfits`)
    .then(data => data.json())
    .then(jsonData => {
        // res.locals.outfitsPids = jsonData.outfits[0].products[0].slotQueue;
        const htwPids = jsonData.outfits[0].products.map(product => product.slotProductId);
        fetchAll(res, next, htwPids, 'outfits')
    });
}

const detail = (req, res, next) => {
    const pid = req.params.pid;
    fetch(`https://api.net-a-porter.com/MRP/GB/en/detail/${pid}`)
    .then(data => data.json())
    .then(jsonData => {
        res.locals.detail = pidReducer(jsonData);
        next();
    });
}

const pidReducer = (pidData) => {
    return {
        name: pidData.name,
        brand: pidData.brand,
        mainImgUrl: getImgUrl(pidData.images.urlTemplate, 'in', 's'),
        mainOutfitUrl: getImgUrl(pidData.images.urlTemplate, 'ou', 'l'),
        id: pidData.id
    };
}


module.exports = {
    init: init,
    ymal:ymal,
    htw: htw,
    fetchAll: fetchAll,
    detail: detail
};
